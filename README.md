# system meta ansible role for linux hosts configuration

## Список компонентов:

| название компонента | функционал                 |
| ------------------- | -------------------------- |
| sysctl              | тюнинг параметров ядра     |
| hostnamectl         | установка имени хоста      |
| resolver            | настройка резолверов       |
| chrony              | настройка служб времени    |
| timezone            | настройка временной зоны   |
| locale              | настройка локали           |
| package             | установка/удаление пакетов |
| docker              | установка docker           |
| unbound             | установка unbound          |

## Описание

Системная мета роль представляет из себя некую логическую сущность/контейнер включающую в себя набор более простых ролей, выполняющих определённую задачу.

Зависимости задаются в meta/main.yml в списке dependencies: [], например:

```
galaxy_info:
  author: Serhio
  description: role to install and configure linux hosts
  company: SomeCompany
  min_ansible_version: 2.8.15
  platforms: []
dependencies:
  - src: https://gitlab.com/ansbl-rls/hostnamectl.git
    name: hostnamectl
    path: roles
    scm: git
    version: master
    when: hostnamectl == true
    tags: ["system", "hostnamectl"]
  - src: https://gitlab.com/ansbl-rls/sysctl.git
    name: sysctl
    path: roles
    scm: git
    version: master
    when: sysctl == true
    tags: ["system", "sysctl"]
```

Это даёт возможность на уровне плейбука и/или хостовых/групповых переменных активировать/деактивировать необходимый функционал:

```
---
all:
  children:
    hosts:
      vm1:
        ansible_python_interpreter: /usr/bin/python3
    vars:
      sysctl: true
      hostnamectl: true
      chrony: true
      timezone: true
      locale: true
      unbound: true
      docker: true
      package: true
      package_install:
        - vim
        - nmap
        - net-tools
        - iotop
        - wget
        - bash-completion
        - sysstat
        - rsync
        - git
        - unzip
        - tcpdump
        - python3-pip
      package_remove:
        - firewalld
```

## локальный тест в vagrant:

```
vagrant up
cd test
ansible-galaxy install -f -r requirements.yml
ansible-playbook -i hosts.yml playbook.yml -u root
```
